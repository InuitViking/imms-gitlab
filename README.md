# Imms/Gitlab

This is a Gitlab plugin for IMMS.

All it (currently) does is look up issues within a group (and its subgroubs) made by a particular user.

To use this, you have to add a new section to the config.ini on IMMS, and fill it with relevant data:

```Ini
[gitlab]
token					= [token with group level access to all its issues]
group_id				= [the ID of the group]
author_id				= [the id of the author]
```

This is made this way, so you can use this plugin for only looking up issues by e.g. the Gitlab Support user (the user used for when people create issues via email to a project).

Since it follows all issues made by this user, it also follows moved issues.
Displays both open and closed issues in separate pages.