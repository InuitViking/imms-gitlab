<?php

namespace Imms\Gitlab;

use Imms\Classes\Bootstrapper;
use Imms\Classes\Cache;
use DateTime;
use Gitlab\Client;
use Gitlab\ResultPager;
use Http\Client\Exception;
use Imms\Gitlab\Extensions\LastTagEditProcessor\LastTagEditProcessor;
use League\CommonMark\Exception\CommonMarkException;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;

class Gitlab {
    public array $subscribedPaths = [
        '/gitlab-issues',
        '/gitlab-cron'
    ];

    public array $subscribedEvents = [
        'add_commonmark_event_listener',
        'git_update'
    ];

    public array $config;
    private Client $client;
    private Cache $cache;
    private string $token;
    private int $groupID;
    private array $issues;
    private string|int $authorID;
    private Filesystem $filesystem;
    private int $limit = 5;

    /**
     * Imms/Gitlab makes it possible to list all open issues from a specified user within a specified group
     */
    public function __construct(Cache $cache) {
        // Set default values
        $this->config = parse_ini_file(Bootstrapper::rootDirectory().'/src/plugins/imms/gitlab/plugin.ini', true);
        $this->token = Bootstrapper::getIni()['gitlab']['token'];
        $this->groupID = Bootstrapper::getIni()['gitlab']['group_id'];
        $this->authorID = Bootstrapper::getIni()['gitlab']['author_id'];

        // We want to make use of the filesystem
        $adapter = new LocalFilesystemAdapter(Gitlab::getPluginDir());
        $this->filesystem = new Filesystem($adapter);

        // Initiate Gitlab Client
        $this->client = new Client();
        $this->client->authenticate($this->token, Client::AUTH_HTTP_TOKEN);

        // If a note has been added to an issue, submit it
        if (isset($_POST['issue_id'])) {
            $body = '**From:** '.$_POST['from_name'].' <'.$_POST['from_email'].'><br><hr>';
            $body .= $_POST['body'];
            $this->client->issues()->addNote($_POST['project_id'], $_POST['issue_id'], $body);
            $this->filesystem->deleteDirectory('tmp/cached_notes');
        }

        // Initiate cache
        $this->cache = $cache;
    }

    /**
     * @param string $event
     * @return void
     */
    public function getNotification (string $event): void {
        if ($event === $this->subscribedEvents[1]) {
            try {
                $this->filesystem->deleteDirectory('tmp');
            } catch (FilesystemException) {
                echo "Failed to delete tmp directory of Gitlab plugin";
            }
        }
    }

    /**
     * @param string $event
     * @return LastTagEditProcessor|null
     */
    public function getCommonMarkEventListener (string $event): ?LastTagEditProcessor {
        if ($event === $this->subscribedEvents[0]) {
            return new LastTagEditProcessor();
        }
        return null;
    }

    /**
     * @param string $event
     * @param array|null $parameters
     * @return array|null
     */
    public function getNotificationWithOutput (string $event, array $parameters = null): ?array {
        if ($event === $this->subscribedEvents[0] && isset($parameters['fullMatch'])) {
            $token = Bootstrapper::getIni()['gitlab']['token'];
            $groupID = Bootstrapper::getIni()['gitlab']['group_id'];

            $project = explode('(', $parameters['fullMatch'])[1];
            $project = explode(')', $project)[0];
            $project = str_replace('/-/badges/release.svg', '', $project);
            $projectArr = explode('/', $project);
            $project = end($projectArr);

            $client = new Client();
            $client->authenticate($token, Client::AUTH_HTTP_TOKEN);

            $projects = $client->groups()->search($groupID, ['scope' => 'projects', 'search' => $project]);
            $projectId = end($projects)['id'];

            $packages = $this->getPackages($projectId, $token);
            $link = $this->getPackageLink(end($packages), $projectId, $token);

            return [
                'type' => 'link',
                'link' => $link,
                'label' => 'Download',
                'title' => 'Download'
            ];
        }
        return null;
    }

    /**
     * Gets all packages from projectID
     *
     * @param int|string $projectId
     * @param string $token
     * @return mixed
     */
    private function getPackages (int|string $projectId, string $token): mixed {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://gitlab.com/api/v4/projects/$projectId/packages?per_page=100&page=1");
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('PRIVATE-TOKEN: ' . $token));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        $result = json_decode(curl_exec($handle), true);
        curl_close($handle);
        return $result;
    }

    /**
     * Generates a generic download link to the package - only works for the generic package registry.
     *
     * Source: https://docs.gitlab.com/ee/api/packages.html
     *
     * @param array $packageMetadata
     * @param int|string $projectId
     * @param string $token
     * @return string
     */
    private function getPackageLink (array $packageMetadata, int|string $projectId, string $token): string {
        $tokenName = Bootstrapper::getIni()['git']['token_name'];
        $tokenPass = Bootstrapper::getIni()['git']['token_pass'];

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://gitlab.com/api/v4/projects/$projectId/packages/" . $packageMetadata['id'] . "/package_files?per_page=100&page=1");
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('PRIVATE-TOKEN: ' . $token));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        $packageFiles = json_decode(curl_exec($handle), true);
        $package = end($packageFiles);
        $link = "https://$tokenName:$tokenPass@gitlab.com/api/v4/projects/$projectId/packages/generic/" . $packageMetadata['name'] . '/' . $package['pipelines'][0]['ref'] . '/' . $package['file_name'];
        curl_close($handle);
        return $link;
    }

    /**
     * @param string $path
     * @return void
     * @throws FilesystemException
     */
    public function getPathNotification (string $path): void {
        if ($path === $this->subscribedPaths[0]) {
            // Whether to show opened or closed issues.
            $state = 'opened';
            if (isset($_GET['closed'])) {
                $state = 'closed';
            }

            // Get all open issues by the specified user
            $pager = new ResultPager($this->client, 100);
            $params = ['author_id'=>$this->authorID, 'state'=>$state];
            try {
                $issueFile = 'tmp/issues.json';
                $issueRender = 'tmp/issues.html';
                if ($state == 'closed') {
                    $issueFile = 'tmp/closed_issues.json';
                    $issueRender = 'tmp/closed_issues.html';
                }

                if (!$this->filesystem->fileExists($issueFile) || (time() - $this->filesystem->lastModified($issueFile)) > 7200) {
                    $this->filesystem->delete($issueRender);
                    $this->filesystem->write($issueFile, json_encode($pager->fetchAll($this->client->groups(), 'issues', [$this->groupID, $params])));
                }
                $this->issues = json_decode($this->filesystem->read($issueFile), true);
                $this->printIssuesPage();
            } catch (CommonMarkException|Exception) {
                echo "Couldn't fetch issues!";
            }
        } elseif ($path === $this->subscribedPaths[1]) {
            $this->filesystem->deleteDirectory('tmp');
            echo "<script>location.href='/';</script>";
        }
    }

    /**
     * Gets a project array from GitLab based on its ID.
     *
     * @param string $projectId
     * @return array
     */
    private function getProject (string $projectId): array {
        try {
            $projectJsonPath = "tmp/project_$projectId.json";
            if (!$this->filesystem->fileExists($projectJsonPath) || (time() - $this->filesystem->lastModified($projectJsonPath)) > 7200) {
                $projectJson = json_encode($this->client->projects()->show($projectId));
                $this->filesystem->write($projectJsonPath, $projectJson);
            }
            return json_decode($this->filesystem->read($projectJsonPath), true);
        } catch (FilesystemException $e) {
            echo $e->getMessage();
            $e->getTraceAsString();
            exit;
        }
    }

    /**
     * @return void
     * @throws CommonMarkException
     * @throws FilesystemException
     */
    private function printIssuesPage (): void {
        if (!isset($_GET['closed'])) {
            echo "<a class='issue-state-link' href='/gitlab-issues?closed'>Closed issues</a>";
        } else {
            echo "<a class='issue-state-link' href='/gitlab-issues'>Opened issues</a>";
        }

        $page = $_GET['page'] ?? 1;
        $pages = ceil(count($this->issues) / $this->limit);

        $this->searchBar();

        if (isset($_GET['giq'])) {
            $query = $_GET['giq'];
            $this->searchIssues($query);
        }

        $issueRenders = $this->getIssueRenders();

        $this->echoPagination($page, $pages);

        if (isset($_GET['page']) && ($_GET['page'] > count($issueRenders) || $_GET['page'] < 1)) {
            echo "<p>Page {$_GET['page']} doesn't exist.</p>";
            return;
        }

        if ($this->filesystem->fileExists($issueRenders[($_GET['page'] ?? 1)]) && (time() - $this->filesystem->lastModified($issueRenders[($_GET['page'] ?? 1)])) < 7200) {
            echo $this->filesystem->read($issueRenders[($_GET['page'] ?? 1)]);
            $this->echoPagination($page, $pages);
            return;
        }

        $htmlArray = $this->renderIssues();

        $count = 1;
        foreach ($htmlArray as $html) {
            if ($count > count($issueRenders)) {
                break;
            }
            $this->filesystem->write($issueRenders[$count], $html);
            $count++;
        }

        echo $this->filesystem->read($issueRenders[($_GET['page'] ?? 1)] );
        $this->echoPagination($page, $pages);
    }

    /**
     * Generates and echos pagination HTML.
     *
     * @param int $page
     * @param int $pages
     * @return void
     */
    private function echoPagination (int $page, int $pages): void {
        $pagination = '<nav><ul class="pagination justify-content-center">';
        $closed = !isset($_REQUEST['closed']) ? '' : "&closed";

        // Previous and first page
        if ($page == 1) {
            $pagination .= "<li class='page-item disabled'><span class='page-link'><<</span></li>";
            $pagination .= "<li class='page-item disabled'><span class='page-link'>←</span></li>";
        } else {
            $firstPageParam = "?page=1$closed";
            $previousPageParam = "?page=" . ($page - 1) . "$closed";
            $pagination .= "<li class='page-item'><a class='page-link' href='$firstPageParam' ><<</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href='$previousPageParam' >←</a></li>";
        }

        // Previous pages
        for ($i = 5; $i >= 1; $i--) {
            $previousPage = $page - $i;
            if ($previousPage >= 1) {
                $previousPagesParam = "?page=$previousPage$closed";
                $pagination .= "<li class='page-item'><a class='page-link' href='$previousPagesParam'>$previousPage</a></li>";
            }
        }

        // Current page
        $pagination .= "<li class='page-item disabled'><span class='page-link'>".$page."</span></li>";

        // Next pages
        for ($i = 1; $i <= 5; $i++) {
            $nextPage = $page + $i;
            if ($nextPage <= $pages) {
                $nextPagesParam = "?page=$nextPage$closed";
                $pagination .= "<li class='page-item'><a class='page-link' href='$nextPagesParam'>$nextPage</a></li>";
            }
        }

        // Next and last page
        if ($page == $pages) {
            $pagination .= "<li class='page-item disabled'><span class='page-link'>→</span></li>";
            $pagination .= "<li class='page-item disabled'><span class='page-link'>>></span></li>";
        } else {
            $lastPageParam = "?page=$pages$closed";
            $nextPageParam = "?page=" . ($page + 1) . "$closed";
            $pagination .= "<li class='page-item'><a class='page-link' href='$nextPageParam' >→</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href='$lastPageParam' >>></a></li>";
        }

        $pagination .= '</ul></nav>';

        echo $pagination;
    }

    /**
     * Get file location of the HTML to use for displaying issues.
     *
     * @return array
     */
    private function getIssueRenders (): array {
        $pages = ceil(count($this->issues) / $this->limit);
        $issueRenders = [];
        for ($i = 1; $i <= $pages; $i++) {
            $issueRenders[$i] = 'tmp/issues.html-' . $i;
            if (isset($_GET['closed'])) {
                $issueRenders[$i] = 'tmp/closed_issues.html-' . $i ;
            }
        }

        return $issueRenders;
    }

    /**
     * Generate and return the HTML for issues.
     *
     * @return array
     * @throws CommonMarkException
     * @throws FilesystemException
     */
    private function renderIssues (): array {
        $count = 0;
        $htmlArray = [];
        $pages = ceil(count($this->issues) / $this->limit);
        $pageCount = 0;
        $html = '';

        foreach ($this->issues as $issue) {
            $project = $this->getProject($issue['project_id']);
            // Print the individual issue, starting with the header
            $issueText = "## #{$issue['id']} - {$issue['title']}\n\n";

            // Create the status string
            $fromString = "[{$issue['service_desk_reply_to']}](mailto:{$issue['service_desk_reply_to']})";
            $createdAt = $this->getFormattedDate($issue['created_at']);
            $updatedAt = $this->getFormattedDate($issue['updated_at']);
            $issueText .= "Issue project ID: {$issue['iid']}\n\n";
            $issueText .= "Project: {$project['name']} ({$project['id']})\n\n";
            $issueText .= "Sent from: $fromString\n\n";
            $issueText .= "Created at: *$createdAt* | Updated at: *$updatedAt*\n\n";

            $html .= $this->cache->md2HtmlNoFile($issueText);

            // Print the assignees
            $html .= $this->getHtmlAssigneesFromIssue($issue);

            // Print the description
            if ($issue['description'] === null) {
                $html .= '<p>No description</p>';
            } else {
                $description = "<details><summary>See description</summary>{$issue['description']}</details>";
                $html .= $this->cache->md2HtmlNoFile($description);
            }

            // Print the notes
            $html .= $this->filesystem->read($this->getCachedNotes($issue));
            $html .= $this->addNoteToIssueForm($issue);

            if ($pageCount == ($pages - 1)) {
                $htmlArray[] = $html;
                $html = '';
                $pageCount++;
                continue;
            }

            if ($count != 0 && $count % $this->limit == 0) {
                $htmlArray[] = $html;
                $html = '';
                $pageCount++;
            }

            $count++;
        }

        return $htmlArray;
    }

    /**
     * Get cached notes, if non-existent, render them, and cache them.
     *
     * @param array $issue
     * @return string
     * @throws CommonMarkException
     * @throws FilesystemException
     */
    private function getCachedNotes (array $issue): string {
        // Created the cached notes dir
        $cachedNotesDir = 'tmp/cached_notes';
        if (!$this->filesystem->directoryExists($cachedNotesDir)) {
            $this->filesystem->createDirectory($cachedNotesDir);
        }

        // Cache the notes if they aren't already, or are over two hours old.
        $cachedNotes = $cachedNotesDir . '/' . $issue['id'] . '_notes.html';
        if (!$this->filesystem->fileExists($cachedNotes) || (time() - $this->filesystem->lastModified($cachedNotes)) >= 7200) {
            $this->filesystem->write($cachedNotes, $this->printNotes($this->getAllNotes($issue)));
        }

        return $cachedNotes;
    }

    /**
     * Display the search bar
     *
     * @return void
     */
    private function searchBar (): void {
        $titleValue = "Search issues using one of the following:\n - Issue title\n - Issue ID\n - Issue ID within project\n - Project ID\n - Project name";
        $value = '';
        if (isset($_GET['giq'])) {
            $value = $_GET['giq'];
        }
        $closedState = '';
        if (isset($_GET['closed'])) {
            $closedState = '<input type="hidden" id="closed" name="closed" value="'.$_GET['closed'].'" />';
        }
        $page = $_GET['page'] ?? 1;
        $pageInput = '<input type="hidden" id="page" name="page" value="'.$page.'" />';
        echo '
<form action="/gitlab-issues" method="get" class="giq-search">
    '.$closedState.'
    '.$pageInput.'
    <input type="text" id="search-issues" name="giq" title="'.$titleValue.'" placeholder="Search issues" value="'.$value.'" />
    <div style="display: inline-block; border: 1px solid gray; padding: 0 5px 0 5px; border-radius: 100px; cursor: help;" title="'.$titleValue.'">?</div>
</form>';
    }

    /**
     * Searches all issues for the query.
     *
     * @param string $query
     * @return void
     */
    private function searchIssues (string $query): void {
        $matches = [];
        $pages = ceil(count($this->issues) / $this->limit);
        $pageCount = 0;
        $count = 0;
        foreach ($this->issues as $issue) {
            $project = $this->getProject($issue['project_id']);
            if (
                (preg_match("/(.*)$query(.*)/i", $issue['title'], $match))
                || (preg_match("/(.*)$query(.*)/i", $issue['id'], $match))
                || (preg_match("/(.*)$query(.*)/i", $issue['iid'], $match))
                || (preg_match("/(.*)$query(.*)/i", $issue['project_id'], $match))
                || (preg_match("/(.*)$query(.*)/i", $project['name'], $match))) {
                $match[0] = $issue['id'] . " - " . $issue['title'];
                $result = preg_replace('/ +/', '-', mb_strtolower($match[0]));
                $result = preg_replace('/-$/', '', $result);
                $result = preg_replace('/^-/', '', $result);
                $result = str_replace([',','.',']','[',':'], '', $result);

                $closed = !isset($_REQUEST['closed']) ? '' : "?closed";
                $searchQuery = '&giq=' . $_REQUEST['giq'] ?? '';
                if ($closed == null) {
                    $searchQuery = '?giq=' . $_REQUEST['giq'] ?? '';
                }
                $page = '&page=' . ($pageCount + 1);

                $result = "$closed$searchQuery$page#$result";
                $matches[$match[0]] = $result;
            }

            if ($pageCount == ($pages - 1)) {
                $pageCount++;
                continue;
            }

            if (($count != 0) && ($count % $this->limit == 0)) {
                $pageCount++;
            }

            $count++;
        }

        echo '<ul id="giq-result">';
        foreach ($matches as $key => $match) {
            echo "<li><a href='$match'>$key</a></li>";
        }
        echo '</ul>';
    }

    /**
     * @param string $date
     * @return string
     */
    private function getFormattedDate (string $date): string {
        $date = DateTime::createFromFormat('Y-m-d\TH:i:s+', $date);
        return $date->format('d-m-y H:i');
    }

    /**
     * @param array $notes
     * @return string
     * @throws CommonMarkException
     */
    private function printNotes (array $notes): string {
        if ($notes === []) {
            $html = '<p><span style="font-size: 11px">▶</span> <i>No notes</i></p>';
        } else {
            $notesSection = '<details><summary style="cursor: ">See notes</summary>';
            foreach ($notes as $note) {
                if ($note->internal === true) {
                    continue;
                }

                $notesSection .= '<div>';
                $notesSection .= "Name: {$note->author->name} - Created at: {$this->getFormattedDate($note->created_at)}

";
                foreach (explode("\n", $note->body) as $line) {
                    $notesSection .= "> $line\n";
                }
            }
            $notesSection .= '</details>';
            $html = $this->cache->md2HtmlNoFile($notesSection);
        }

        return $html;
    }

    /**
     * Adds a form under each issue, that makes it possible to add a note to the issue.
     *
     * @param array $issue
     * @return string
     */
    private function addNoteToIssueForm (array $issue): string {
        return "<details>
                    <summary>Add note</summary>
                    <form method='post' action='' class='addNoteForm'>
                        <input type='hidden' name='issue_id' value='{$issue['iid']}' />
                        <input type='hidden' name='project_id' value='{$issue['project_id']}' />
                        <input type='text' name='from_name' placeholder='Your name' /><br>
                        <input type='email' name='from_email' placeholder='Your email' /><br>
                        <textarea name='body' rows='10' cols='50'></textarea><br>
                        <input type='submit' value='Add note' />
                    </form>
                </details>";
    }

    /**
     * @param array $issue
     * @return array
     * @throws FilesystemException
     */
    private function getAllNotes (array $issue): array {
        $notePath = 'tmp/'.$issue['id'].'_notes.json';
        if (!$this->filesystem->fileExists($notePath) || (time() - $this->filesystem->lastModified($notePath)) >= 7200) {
            $notesJson = json_encode($this->client->issues()->showNotes($issue['project_id'], $issue['iid']));
            $this->filesystem->write($notePath, $notesJson);
        }
        return json_decode($this->filesystem->read($notePath));
    }

    /**
     * @param array $issue
     * @return string
     * @throws CommonMarkException
     */
    private function getHtmlAssigneesFromIssue (array $issue): string {
        $assignees = $issue['assignees'];

        $assigneeText = '';

        if (count($assignees) === 1) {
            $assigneeText = $issue['assignee']['name'];
        } elseif ($assignees === []) {
            $assigneeText = 'None';
        } else {
            foreach ($assignees as $assignee) {
                if ($assignees[0] === $assignee) {
                    $assigneeText .= "{$assignee['name']}";
                } else {
                    $assigneeText .= ", {$assignee['name']}";
                }
            }
        }

        return $this->cache->md2HtmlNoFile("Assignees: $assigneeText");
    }

    /**
     * Simply returns the root directory of the plugin as a string.
     *
     * @return string
     */
    public static function getPluginDir (): string {
        return Bootstrapper::rootDirectory().'/src/plugins/imms/gitlab';
    }
}
