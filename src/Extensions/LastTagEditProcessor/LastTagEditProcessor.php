<?php

declare(strict_types=1);

namespace Imms\Gitlab\Extensions\LastTagEditProcessor;

use Gitlab\Client;
use Imms\Classes\Bootstrapper;
use Imms\Gitlab\Gitlab;
use League\CommonMark\Event\DocumentParsedEvent;
use League\CommonMark\Extension\CommonMark\Node\Inline\Link;
use League\CommonMark\Node\Inline\Text;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;

// https://github.com/thephpleague/commonmark/discussions/1028

class LastTagEditProcessor {
    public function onDocumentParsed(DocumentParsedEvent $event): void {
        $document = $event->getDocument();
        $walker = $document->walker();
        while ($event = $walker->next()) {
            $node = $event->getNode();

            // Only stop at Link nodes when we first encounter them
            if (!($node instanceof Link) || !$event->isEntering()) {
                continue;
            }

            $literal = "";
            foreach ($node->children() as $child) {
                if ($child instanceof Text) {
                    $literal = $child->getLiteral();
                }
            }

            if ($this->isLiteralVersion($literal)) {
                $node->setTitle($literal);
                $node->data->append('attributes/class', 'external-link');

                // Prepare the filesystem
                $adapter = new LocalFilesystemAdapter(Gitlab::getPluginDir());
                $filesystem = new Filesystem($adapter);

                // Get the project name
                $project = $node->getUrl();
                $project = current(explode('/-', $project));
                $projectArr = explode('/', $project);
                $project = end($projectArr);

                // If the file exists and is younger than two hours, get the version from the file; else find the new version
                if ($filesystem->fileExists("tmp/$project") && (time() - $filesystem->lastModified("tmp/$project")) < 7200) {
                    $node->setUrl($filesystem->read("tmp/$project"));
                } else {
                    $node->setUrl($this->getUpdatedLink($project, $filesystem));
                }
            }
        }
    }

    /**
     * Get the new link and update it
     *
     * @param string $project
     * @param Filesystem $filesystem
     * @return string
     */
    public static function getUpdatedLink (string $project, Filesystem $filesystem): string {
        $token = Bootstrapper::getIni()['gitlab']['token'];
        $groupID = Bootstrapper::getIni()['gitlab']['group_id'];

        $client = new Client();
        $client->authenticate($token, Client::AUTH_HTTP_TOKEN);

        $projectId = $client->groups()->search($groupID, ['scope' => 'projects', 'search' => $project])[0]['id'];

        $packages = (new LastTagEditProcessor)->getPackages($projectId, $token);
        $link = (new LastTagEditProcessor)->getPackageLink(end($packages), $projectId, $token);

        try {
            $filesystem->createDirectory('tmp');
            $filesystem->write("tmp/$project", $link);
        } catch (FilesystemException) {
            echo "Couldn't create tmp/$project.";
        }

        return $link;
    }

    /**
     * Checks whether a literal (string) is a version string.
     *
     * @param string $literal
     * @return bool
     */
    private function isLiteralVersion(string $literal): bool {
        // Only look at http and https URLs
        if (!preg_match("/^(?'MAJOR'0|[1-9]\d*)\.(?'MINOR'0|[1-9]\d*)\.(?'PATCH'0|[1-9]\d*)(?:-(?'prerelease'(?:0|[1-9A-Za-z-][0-9A-Za-z-]*)(?:\.(?:0|[1-9A-Za-z-][0-9A-Za-z-]*))*))?(?:\+(?'build'(?:0|[1-9A-Za-z-][0-9A-Za-z-]*)(?:\.(?:0|[1-9A-Za-z-][0-9A-Za-z-]*))*))?$/i", $literal)) {
            return false;
        }

        return true;
    }

    /**
     * Gets all packages from projectID
     *
     * @param int|string $projectId
     * @param string $token
     * @return mixed
     */
    private function getPackages (int|string $projectId, string $token): mixed {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://gitlab.com/api/v4/projects/$projectId/packages?per_page=100");
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('PRIVATE-TOKEN: ' . $token));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        $result = json_decode(curl_exec($handle), true);

        foreach ($result as $key => $package) {
            if (isset($package['pipeline']) && (str_contains($package['pipeline']['ref'], '-rc') || str_contains($package['pipeline']['ref'], '-RC'))) {
                unset($result[$key]);
            }
        }

        curl_close($handle);
        return $result;
    }

    /**
     * Generates a generic download link to the package - only works for the generic package registry.
     *
     * Source: https://docs.gitlab.com/ee/api/packages.html
     *
     * @param array $packageMetadata
     * @param int|string $projectId
     * @param string $token
     * @return string
     */
    private function getPackageLink (array $packageMetadata, int|string $projectId, string $token): string {
        $tokenName = Bootstrapper::getIni()['git']['token_name'];
        $tokenPass = Bootstrapper::getIni()['git']['token_pass'];

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://gitlab.com/api/v4/projects/$projectId/packages/" . $packageMetadata['id'] . "/package_files");
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('PRIVATE-TOKEN: ' . $token));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        $packageFiles = json_decode(curl_exec($handle), true);
        $package = end($packageFiles);
        $link = "https://$tokenName:$tokenPass@gitlab.com/api/v4/projects/$projectId/packages/generic/" . $packageMetadata['name'] . '/' . $package['pipelines'][0]['ref'] . '/' . $package['file_name'];
        curl_close($handle);
        return $link;
    }
}